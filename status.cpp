#include "status.h"
#include "iri.h"
#include <math.h>
#include <BH1750.h>

BH1750 light;
SoftwareWire light_soft_wire(LIGHT_SDA, LIGHT_SCL);
SoftwareWire th_soft_wire(TH_SDA, TH_SCL);
DallasTemp ds(WATER_TEMP);

extern LiquidCrystal_I2C lcd;

bool Status::light_begin() {
  light_soft_wire.begin();
  return light.begin(light_soft_wire);
}

float Status::light_read() {
  float lux = light.readLightLevel();
  if (lux < 0) {
    light_begin();
    return 0;
  } else {
    return lux;
  }
}

Status::Status() {
  this->th_status = false;
  set_light_checker(true, 3333, "Configure light");
  set_dist_checker(true, 100, "Configure water");
  water_temp_check_enabled = true;
  th_check_enabled = true;
  th_init();
  light_begin();

  worker();
}

float Status::get_temperature() {
  if (th_status) {
    float temp = th_sensor.readTemperature();
    if (temp < -140) {
      th_status = false;
      return MINVALUE;
    }
    return th_sensor.readTemperature();
  } else {
    return MINVALUE;
  }
}

float Status::get_humidity() {
  if (th_status) {
    return th_sensor.readHumidity();
  } else {
    return MINVALUE;
  }
}

void Status::th_init() {
  if (!th_status) {
    th_soft_wire.begin();
    unsigned status = th_sensor.begin(0x76, &th_soft_wire);
    if (!status) {
      th_status = false;
    } else {
      th_status = true;
    }
  }
}

void Status::show_fourth_row(String ok_row) {
  if (semaphore) {
    emit(ok_row);
  } else {
    emit(semaphore_msg);
  }
}

void Status::lcd_text() {
  lcd.clear();
  lcd.setCursor(0, 0);
  String first_row = String("Light: ") + String(_light);
  emit(first_row);
  lcd.setCursor(0, 1);
  String second_row = String("Temp: ") + String(round(get_temperature())) + (char)223 + String("C ") + String("Hum: ") + String(round(get_humidity())) + String("%");
  emit(second_row);
  lcd.setCursor(0, 2);
  String third_row = String("Dst: ") + String(round(ultrasound_ping())) + String("cm") + String(" T: ") + String(round(_water_temp)) + (char)223 + String("C");
  emit(third_row);
  lcd.setCursor(0, 3);
  String fourth_row = ok_msg;
  show_fourth_row(fourth_row);
}

float Status::water_temp_read() {
  ds.measure(true);
  return ds.read();
}


void Status::worker() {
  _distance = ultrasound_ping();
  Serial.print("D: usnd: ");
  Serial.println(_distance);
  _light = light_read();
  Serial.print("D: light: ");
  Serial.println(_light);
  _water_temp = water_temp_read();
  Serial.print("D: water temp: ");
  Serial.println(_water_temp);
  check_semaphore();
}

void Status::set_light_checker(bool enabled, float thr, const char * errmsg) {
  light_check_enabled = enabled;
  light_check_thr = thr;
  light_check_errmsg = String(errmsg);
}

void Status::set_dist_checker(bool enabled, float thr, const char * errmsg) {
  dist_check_enabled = enabled;
  dist_check_thr = thr;
  dist_check_errmsg = String(errmsg);
}

void Status::set_ok_msg(const char * _ok_msg) {
  ok_msg = String(_ok_msg);
}


bool Status::check_semaphore() {
  bool _status;

  _status = true;
  semaphore_msg = ok_msg;

  if (light_check_enabled && (light_check_thr < _light)) {
    _status = false;
    semaphore_msg = light_check_errmsg;
  }
  if (dist_check_enabled) {
    if (dist_check_thr < _distance) {
      _status = false;
      semaphore_msg = dist_check_errmsg;
    } else if (_distance < 0)  {
      _status = false;
      semaphore_msg = "Water sensor error";
    }
  }
  if (th_check_enabled) {
    float temp = get_temperature();
    if (temp > 50 || temp < 0) {
      _status = false;
      semaphore_msg = "BME sensor error";
    }
    float hum = get_humidity();
    if (hum > 100 || hum < 0) {
      _status = false;
      semaphore_msg = "BME sensor error";
    }
  }
  if (water_temp_check_enabled) {
    float water_temp = water_temp_read();
    if (water_temp > 50 || water_temp < 0) {
      _status = false;
      semaphore_msg = "Water temp error";
    }
  }

  this->semaphore = _status;
  return _status;
}

float Status::ultrasound_ping_inner() {
  unsigned long timeStart;
  unsigned long timeIn;
  unsigned long timeOut;
  pinMode(USND_TRIGGER, OUTPUT);
  pinMode(USND_ECHO, INPUT);
  usnd_state = LOW;

  digitalWrite(USND_TRIGGER, LOW);
  delayMicroseconds(2);
  digitalWrite(USND_TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(USND_TRIGGER, LOW);

  timeStart = micros();

  while (usnd_state == LOW && (micros() - timeStart < 40000)) {
    usnd_state = digitalRead(USND_ECHO);
  }
  timeIn = micros();

  while (usnd_state == HIGH && (micros() - timeStart < 40000)) {
    usnd_state = digitalRead(USND_ECHO);
  }
  timeOut = micros();

  if (timeOut - timeStart >= 40000) {
    return -1;
  }
  return timetocm(timeOut - timeIn);
}

float Status::timetocm(long mtime) {
  float mtime_f = (float) mtime;
  if (mtime > 0) {
    return mtime_f / 27 / 2;
  } else {
    return MINVALUE;
  }
}

float Status::ultrasound_ping() {
  float distance;
  distance = MINVALUE;
  for (int i = 0; (i < 5 && isinf(distance)) ; i++) {
    delay(10);
    distance = ultrasound_ping_inner();
  }
  return distance;
}
