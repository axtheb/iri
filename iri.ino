#include "iri.h"
#include "Arduino.h"

LiquidCrystal_I2C lcd(0x27, 20, 4);  // set the LCD address to 0x27 or 0x3f

#include "flowerpot.h"
#include "settings.h"

#include<avr/wdt.h>

// for compiler
#include <LiquidCrystal_I2C.h>
#include <SoftwareWire.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <DallasTemperature.h>

Flowerpot *flowerpots[FLOWERPOTS];

Status *status;

int loop_timer = 0;

void emit(String row) {
  Serial.print("emit: ");
  lcd.print(row);
  Serial.println(row);
}

void setup() {
  wdt_disable();
  Serial.begin(115200);
  lcd.init();  // initialize the lcd
  lcd.backlight();  // turn backlight on
  lcd.begin(20, 4);
  lcd.setCursor(4, 1);
  emit("Let it grow!");
  delay(1000);
  wdt_enable(WDTO_8S);
  status = new Status();
  set_status(status);
  for (int i = 0; i < FLOWERPOTS; i++) {
    wdt_reset();
    flowerpots[i] = new Flowerpot(i, status);
    flowerpots[i]->reset();
  }
  set_flowerpots(flowerpots);
}

int lcd_index = FLOWERPOTS;  // which flowerpot to display
int lcd_counter = 0;  // counter: seconds to flip pages
int lcd_counter_start = 5;


void loop() {  // cppcheck-suppress unusedFunction
  wdt_reset();  // Reset the watchdog
  status->worker();
  for (int i = 0; i < FLOWERPOTS; i++) {
    flowerpots[i]->tick_timers();
  }
  for (int j = 0; j < MEASUREMENTS; j++) {
    for (int i = 0; i < FLOWERPOTS; i++) {
      flowerpots[i]->measure();
    }
    wdt_reset();
    delay(100);
  }
  for (int i = 0; i < FLOWERPOTS; i++) {
    flowerpots[i]->worker();
  }
  if (false) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(flowerpots[0]->lcd_text_short());
    lcd.setCursor(0, 1);
    lcd.print(flowerpots[1]->lcd_text_short());
  } else {
    if (lcd_counter == 0) {
      lcd_counter = lcd_counter_start;
      lcd_index++;
      if (lcd_index > FLOWERPOTS) {
        lcd_index = 0;
      }
    }
    Serial.println(lcd_index);
    if (lcd_index == 0) {
      status->lcd_text();
    } else {
      flowerpots[lcd_index - 1]->lcd_text(&lcd);
    }
    lcd_counter--;
  }
  loop_timer = 1000 - (millis() % 1000);
  Serial.print("Sleeping for ");
  Serial.println(loop_timer);
  delay(loop_timer);
}
