#pragma once
#include "iri.h"
#include <LiquidCrystal_I2C.h>

class Flowerpot {
  public:
    Flowerpot(int index, Status * status);
    void measure();  // reads sensor
    int get_reading();
    void reset();
    void setup(int AIR, int WATER);
    int classify_humidity(int humidity);

    void set_sensor_pin(int pin);
    void set_relay_pin(int pin);
    String lcd_text_short();
    String name;
    void set_name(String name);
    void lcd_text(LiquidCrystal_I2C * lcd);
    int get_percent();

    void set_timers(
        int _prewater_on_timer,
        int prewater_off_timer,
        int _on_timer,
        int _off_timer,
        int _measure_timer,
        int _measure_failure_delay);

    void set_timers();

    void tick_timers();
    void worker();

    Status * status;

  private:
    void switch_on();
    void switch_off();
    bool running();
    int sensor_pin;
    int relay_pin;

    String texts[STATES];

    int raw_soil_moisture;
    int mapped_soil_moisture;

    int humidity_cache[MEASUREMENTS];
    // int percent;
    int index;
    int idx;
    int air;
    int water;
    int state;

    /* timery. Pokud nejsou na nule tak se každou sekundu o jedna sniží.
       Pokud klesne on_timer na nulu, příslušný výstup se vypne (a nastaví se
       off_timer). Pokud off timer není nula, nesmí se výstup zapnout.
       measure_timer udává rozestup mezi jednotlivými měřeními
       measure_failure_delay udává, za jak dlouho se má měřit znovu, pokud se mělo ale nesmělo zalévat
    */

    int prewater_on_timer;
    int prewater_on_timer_start;
    int prewater_off_timer;
    int prewater_off_timer_start;
    int on_timer;
    int on_timer_start;
    int off_timer;
    int off_timer_start;
    int measure_timer;
    int measure_timer_start;
    int measure_failure_delay;
};
