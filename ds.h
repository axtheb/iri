#pragma once

#include <OneWire.h>
#include <DallasTemperature.h>

class DallasTemp {
    byte result;
    byte addr[8];

  public:
    explicit DallasTemp(int pin);
    void do_search();
    void measure(bool);
    float read();
  private:
    OneWire* wire;
    DallasTemperature* sensor;
    float inner_read();
};
