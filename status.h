#pragma once
// Not used, but needed by Adafruit_BME280.h
#include <SPI.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

class Status {
  public:
    Status();
    bool can_water();
    void measure();
    float get_temperature();
    float get_humidity();
    float get_distance();
    float light_read();
    bool light_begin();
    void lcd_text();
    void worker();
    bool check_semaphore();
    void set_light_checker(bool enabled, float thr, const char * errmsg);
    void set_dist_checker(bool enabled, float thr, const char * errmsg);
    float water_temp_read();
    void show_fourth_row(String ok_row);
    void set_ok_msg(const char * _ok_msg);

    bool semaphore;

    bool th_check_enabled;
    bool water_temp_check_enabled;


  private:
    String semaphore_msg;
    float _water_temp;
    int usnd_state;
    float ultrasound_ping();
    float ultrasound_ping_inner();
    void th_init();
    bool th_status;
    float timetocm(long mtime);
    Adafruit_BME280 th_sensor;
    float _distance;
    float _light;
    bool light_check_enabled;
    float light_check_thr;
    String light_check_errmsg;
    bool dist_check_enabled;
    float dist_check_thr;
    String dist_check_errmsg;
    String ok_msg;
};
