#include "iri.h"
#include "flowerpot.h"

int return_middle(int values[]) {
  int tmpval;
  if (values[0] > values[1]) {
    tmpval = values[1];
    values[1] = values[0];
    values[0] = tmpval;
  }

  if (values[1] > values[2]) {
    tmpval = values[2];
    values[2] = values[1];
    values[1] = tmpval;
  }

  if (values[0] > values[1]) {
    tmpval = values[1];
    values[1] = values[0];
    values[0] = tmpval;
  }

  return values[1];
}

Flowerpot::Flowerpot(int index, Status * status) {
  this->index = index + 1;
  this->status = status;
  // If sensors and relays are in sequence, we can setup them automatically
  set_sensor_pin(SENSOR_START + index);
  set_relay_pin(RELAY_START + index);

  set_timers();

  reset();
  setup(300, 650);

  texts[DRY] = DRY_TEXT;
  texts[NEAR_DRY] = NEAR_DRY_TEXT;
  texts[WET] = WET_TEXT;
  texts[VERY_WET] = VERY_WET_TEXT;
  texts[ERR] = ERR_TEXT;
  state = ERR;
}

void Flowerpot::set_timers(
  int _prewater_on_timer,
  int _prewater_off_timer,
  int _on_timer,
  int _off_timer,
  int _measure_timer,
  int _measure_failure_delay
) {
  prewater_on_timer_start = _prewater_on_timer;
  if (prewater_on_timer > prewater_on_timer_start) {
    prewater_on_timer = prewater_on_timer_start;
  }
  prewater_off_timer_start = _prewater_off_timer;
  if (prewater_off_timer > prewater_off_timer_start) {
    prewater_off_timer = prewater_off_timer_start;
  }
  on_timer_start = _on_timer;
  if (on_timer > on_timer_start) {
    on_timer = on_timer_start;
  }
  off_timer_start = _off_timer;
  if (off_timer > off_timer_start) {
    off_timer = off_timer_start;
  }
  measure_timer_start = _measure_timer;
  if (measure_timer > measure_timer_start) {
    measure_timer = measure_timer_start;
  }
  measure_failure_delay = _measure_failure_delay;
}

void Flowerpot::set_timers() {
  set_timers(0, 0, 10, 10, 5, 2);
}

void Flowerpot::reset() {
  // Set safe defaults
  idx = 0;
  for (int i = 0; i < MEASUREMENTS; i++) {
    humidity_cache[i] = 100;
  }
  prewater_on_timer = 0;
  prewater_off_timer = 0;
  on_timer = 0;
  off_timer = 0;
  switch_off();
  measure_timer = 1;  // 1 tick will decrease it to 0 and then measure will run
}

void Flowerpot::set_sensor_pin(int pin) {
  sensor_pin = pin;
}

void Flowerpot::set_relay_pin(int pin) {
  relay_pin = pin;
  pinMode(relay_pin, OUTPUT);
}

void Flowerpot::setup(int AIR, int WATER) {
  // nastavi kalibracni hodnoty
  air = AIR;
  water = WATER;
}

void Flowerpot::measure() {
  // nacte mereni do round robin bufferu
  if (measure_timer == 0) {
    int _readings[3];
    analogRead(sensor_pin);
    delay(5);
    for (int i = 0; i < 3; i++) {
      _readings[i] = analogRead(sensor_pin);
      delay(5);
    }
    int _vlhkost = return_middle(_readings);

    this->raw_soil_moisture = _vlhkost;

    int mapped_value = map(_vlhkost, air, water, 0, 100);

    this->mapped_soil_moisture = mapped_value;

    humidity_cache[idx] = mapped_value;
    if (DEBUG) {
      Serial.print("Sensor ");
      Serial.print(index);
      Serial.print(" raw read: ");
      Serial.print(_readings[0]);
      Serial.print(" ");
      Serial.print(_readings[1]);
      Serial.print(" ");
      Serial.print(_readings[2]);
      Serial.print(" median: ");
      Serial.print(_vlhkost);
      Serial.print(" mapped: ");
      Serial.print(mapped_value);
      Serial.println("");
    }
    idx++;
    if (idx == MEASUREMENTS) {
      idx = 0;
    }
  }
}


int Flowerpot::get_reading() {
  // sortne rr buffer a vrati prostredni hodnotu
  int _vlhkost[MEASUREMENTS];
  for (int i = 0; i < MEASUREMENTS; i++) {
    _vlhkost[i] = humidity_cache[i];
  }
  return return_middle(_vlhkost);
}

int Flowerpot::classify_humidity(int humidity_percent) {
  if ((humidity_percent < 0) || (humidity_percent > 100)) {
    return ERR;
  }
  if (humidity_percent <= 25) {
    return DRY;
  }
  if (humidity_percent <= 50) {
    return NEAR_DRY;
  }
  if (humidity_percent <= 75) {
    return WET;
  }
  return VERY_WET;
}

void Flowerpot::tick_timers() {
  if (prewater_on_timer > 0) {  // prewatering
    if (--prewater_on_timer == 0) {
      switch_off();
      prewater_off_timer = prewater_off_timer_start;
    } else {
      switch_on();
    }
  } else if (prewater_off_timer > 0) {  // pause after prewatering
    if (--prewater_off_timer == 0) {
      switch_on();
      on_timer = on_timer_start;
    } else {
      switch_off();
    }
  } else if (on_timer > 0) {  // main watering
    if (--on_timer == 0) {
      switch_off();
      off_timer = off_timer_start;
    } else {
      switch_on();
    }
  } else if (off_timer > 0) {
    // pause after main watering. No water, no measurement.
    switch_off();
    if (--off_timer == 0) {
      measure_timer = 0;
    }
  } else if (measure_timer > 0) {
    switch_off();
    measure_timer--;
  } else {
    switch_off();
    measure_timer = measure_timer_start;
  }
}

String Flowerpot::lcd_text_short() {
  String text;
  int _percent = get_reading();
  int _humidity = classify_humidity(_percent);
  text = String(_percent) + String("%") + texts[_humidity] + String(" ") + String(raw_soil_moisture) + String(":") + String(mapped_soil_moisture);
  if (on_timer) text += String("w ") + String(on_timer) + String(" ");
  text += String(measure_timer);
  return text;
}

void Flowerpot::set_name(String name) {
  this->name = name;
}

void Flowerpot::lcd_text(LiquidCrystal_I2C * lcd) {
  int percent = get_reading();
  int humidity = classify_humidity(percent);
  lcd->clear();
  lcd->setCursor(0, 0);
  String first_row;
  if (index < 10) {
    first_row = String("0") + String(index);
  } else {
    first_row = String(index);
  }
  first_row += String(" ") + String(name);
  emit(first_row);
  lcd->setCursor(0, 1);
  String second_row = String(percent) + String("%") + texts[humidity] + String(" ") + String(raw_soil_moisture) + String(":") + String(mapped_soil_moisture);
  emit(second_row);
  lcd->setCursor(0, 2);
  String third_row;

  if (prewater_on_timer > 0) {
    third_row = String("PreWater: ") + String(prewater_on_timer);
  } else if (prewater_off_timer > 0) {
    third_row = String("PreSoak: ") + String(prewater_off_timer);
  } else if (on_timer > 0) {
    third_row = String("Watering: ") + String(on_timer);
  } else if (off_timer > 0) {
    third_row = String("Soaking: ") + String(off_timer);
  } else if (measure_timer > 0) {
    third_row = String("Waiting: ") + String(measure_timer);
  }

  emit(third_row);
  lcd->setCursor(0, 3);
  String fourth_row = String("undefined");
  if (prewater_on_timer > 0) {
    fourth_row = String("Pump run(pre)..");
  } else if (prewater_off_timer > 0) {
    fourth_row = String("Soaking up(pre)..");
  } else if (on_timer > 0) {
    fourth_row = String("Pump run...");
  } else if (off_timer > 0) {
    fourth_row = String("Soaking up..");
  } else if (measure_timer > 0) {
    fourth_row = String("Waiting..");
  }
  this->status->show_fourth_row(fourth_row);
}

bool Flowerpot::running() {
  return (on_timer > 0 || off_timer > 0 || prewater_on_timer > 0 || prewater_off_timer > 0);
}

void Flowerpot::switch_on() {
  if (DEBUG) {
    Serial.println("D: switching on");
  }
  digitalWrite(relay_pin, HIGH);
}

void Flowerpot::switch_off() {
  if (DEBUG) {
    Serial.println("D: switching off");
  }
  digitalWrite(relay_pin, LOW);
}


void Flowerpot::worker() {
  if (DEBUG) {
    Serial.println();
    Serial.print("D: Timers for ");
    Serial.print(index);
    Serial.println(":");
    Serial.print("prewater: ");
    Serial.print(prewater_on_timer);
    Serial.print(" prewater off: ");
    Serial.print(prewater_off_timer);
    Serial.print(" on: ");
    Serial.print(on_timer);
    Serial.print(" off:");
    Serial.print(off_timer);
    Serial.print(" measure: ");
    Serial.println(measure_timer);
    Serial.println();
  }

  state = classify_humidity(get_reading());
  if (!this->status->semaphore) {
    state = ERR;
  }

  if (!running()) {
    if (state == DRY) {
      if (prewater_on_timer_start > 0) {
        prewater_on_timer = prewater_on_timer_start;
      } else {
        on_timer = on_timer_start;
      }
      // do not moeasure soil humidity during watering
      measure_timer = prewater_on_timer_start + prewater_off_timer_start + on_timer_start + off_timer_start;
      switch_on();
    }
  }
  if (state == ERR) {
    switch_off();
    reset();
  }
}
