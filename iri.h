#pragma once
#include "Arduino.h"
#include <math.h>

#define MINVALUE -INFINITY
#define DEBUG true
#define FLOWERPOTS 8
#define MEASUREMENTS 3

#define LIGHT_SDA 22
#define LIGHT_SCL 23

#define TH_SDA 24
#define TH_SCL 25

#define WATER_TEMP 26

#define USND_TRIGGER 28
#define USND_ECHO 29
#define STATES 5

#define SENSOR_START A0
#define RELAY_START 30

#define DRY 0
#define DRY_TEXT String(" Dry ")
#define NEAR_DRY 1
#define NEAR_DRY_TEXT String(" Near dry ")
#define WET 2
#define WET_TEXT String(" Wet ")
#define VERY_WET 3
#define VERY_WET_TEXT String(" Very wet ")
#define ERR 4
#define ERR_TEXT String(" Err ")

void emit(String row);

#include "status.h"
#include "settings.h"
#include "flowerpot.h"
#include "ds.h"

#include <LiquidCrystal_I2C.h>
#include <SoftwareWire.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <DallasTemperature.h>
