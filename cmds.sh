cppcheck --enable=all --inline-suppr --language=c++ *.ino *.cpp *.h
cpplint --filter=-legal/copyright,-build/include_subdir,-build/include_order,-whitespace/indent,-readability/casting,-runtime/int --extensions=c,c++,cc,cpp,cu,cuh,cxx,h,h++,hh,hpp,hxx,ino *.ino *.cpp *.h
