# Arduino Make file. Refer to https://github.com/sudar/Arduino-Makefile

# if you have placed the alternate core in your sketchbook directory, then you can just mention the core name alone.
#ALTERNATE_CORE = attiny
# If not, you might have to include the full path.
#ALTERNATE_CORE_PATH = /home/sudar/Dropbox/code/arduino-sketches/hardware/attiny/

ARDUINO_LIBS = Adafruit_BusIO Adafruit_BME280  Adafruit_Sensor  BH1750  DallasTemperature  LiquidCrystal_I2C  OneWire  SoftwareWire SPI Wire

BOARD_TAG    = mega
BOARD_SUB    = atmega2560
ifneq (,$(wildcard /dev/ttyUSB0))
	MONITOR_PORT = /dev/ttyUSB0
else 
	MONITOR_PORT = /dev/ttyACM0
endif

include $(ARDMK_DIR)/Arduino.mk

# !!! Important. You have to use make ispload to upload when using ISP programmer
